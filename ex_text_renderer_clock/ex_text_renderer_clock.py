# coding=utf-8
"""
Daniel Calderon, CC3501, 2020-1
Example drawing text with OpenGL textures
"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
import datetime
import random

import transformations as tr
import basic_shapes as bs
import easy_shaders as es
import text_renderer as tx

# A class to store the application control
class Controller:
    def __init__(self):
        self.fillPolygon = True


# global controller as communication with the callback function
controller = Controller()

def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_ESCAPE:
        sys.exit()


if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 600
    height = 600

    window = glfw.create_window(width, height, "Rendering text with OpenGL textures", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Creating shader programs
    texturePipeline = es.SimpleTextureShaderProgram()
    textPipeline = tx.TextureTextRendererShaderProgram()

    # Setting up the clear screen color
    glClearColor(0.25, 0.25, 0.25, 1.0)

    # Enabling transparencies
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # Creating texture with all characters
    textBitsTexture = tx.generateTextBitsTexture()
    # Moving texture to GPU memory
    gpuText3DTexture = tx.toOpenGLTexture(textBitsTexture)

    # Testing text 3D texture. Check the terminal!
    for char in "hi!":
        print(textBitsTexture[:, :, ord(char)] * 255)
        print()

    # Creating shapes on GPU memory

    backgroundShape = bs.createTextureQuad()
    bs.scaleVertices(backgroundShape, 5, [2,2,1])
    backgroundTexture = es.textureWithMipMapSetup("torres-del-paine-sq.jpg")
    gpuBackground = es.toGPUShape(backgroundShape, GL_STATIC_DRAW, backgroundTexture)

    headerText = "Torres del Paine"
    headerCharSize = 0.1
    headerCenterX = headerCharSize * len(headerText) / 2
    headerShape = tx.textToShape(headerText, headerCharSize, headerCharSize)
    gpuHeader = es.toGPUShape(headerShape, GL_STATIC_DRAW, gpuText3DTexture)
    headerTransform = tr.matmul([
        tr.translate(0.9, -headerCenterX, 0),
        tr.rotationZ(np.pi/2),
    ])

    dateCharSize = 0.15
    timeCharSize = 0.1

    now = datetime.datetime.now()
    dateStr = now.strftime("%d/%m/%Y")
    timeStr = now.strftime("%H:%M:%S.%f")[:-3]
    dateShape = tx.textToShape(dateStr, dateCharSize, dateCharSize)
    timeShape = tx.textToShape(timeStr, timeCharSize, timeCharSize)
    gpuDate = es.toGPUShape(dateShape, GL_DYNAMIC_DRAW, gpuText3DTexture)
    gpuTime = es.toGPUShape(timeShape, GL_DYNAMIC_DRAW, gpuText3DTexture)

    second = now.second
    color = [1.0,1.0,1.0]

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)

        glUseProgram(texturePipeline.shaderProgram)
        texturePipeline.drawShape(gpuBackground)

        glUseProgram(textPipeline.shaderProgram)
        glUniform4f(glGetUniformLocation(textPipeline.shaderProgram, "fontColor"), 1,1,1,0)
        glUniform4f(glGetUniformLocation(textPipeline.shaderProgram, "backColor"), 0,0,0,1)
        glUniformMatrix4fv(glGetUniformLocation(textPipeline.shaderProgram, "transform"), 1, GL_TRUE, headerTransform)
        textPipeline.drawShape(gpuHeader)

        now = datetime.datetime.now()
        dateStr = now.strftime("%d/%m/%Y")
        timeStr = now.strftime("%H:%M:%S.%f")[:-3]
        dateShape = tx.textToShape(dateStr, dateCharSize, dateCharSize)
        timeShape = tx.textToShape(timeStr, timeCharSize, timeCharSize)

        es.updateGPUShape(gpuDate, dateShape, GL_DYNAMIC_DRAW)
        es.updateGPUShape(gpuTime, timeShape, GL_DYNAMIC_DRAW)

        if now.second != second:
            second = now.second
            color = [random.random(), random.random(), random.random()]
        
        glUniform4f(glGetUniformLocation(textPipeline.shaderProgram, "fontColor"), color[0], color[1], color[2], 1)
        glUniform4f(glGetUniformLocation(textPipeline.shaderProgram, "backColor"), 1-color[0], 1-color[1], 1-color[2],0.5)
        glUniformMatrix4fv(glGetUniformLocation(textPipeline.shaderProgram, "transform"), 1, GL_TRUE,
            tr.translate(-0.9, -0.7, 0))
        textPipeline.drawShape(gpuDate)

        glUniform4f(glGetUniformLocation(textPipeline.shaderProgram, "fontColor"), 1,1,1,1)
        glUniform4f(glGetUniformLocation(textPipeline.shaderProgram, "backColor"), 0,0,0,0)
        glUniformMatrix4fv(glGetUniformLocation(textPipeline.shaderProgram, "transform"), 1, GL_TRUE,
            tr.translate(-0.9, -0.9, 0))
        textPipeline.drawShape(gpuTime)

        # Once the drawing is rendered, buffers are swap so an uncomplete drawing is never seen.
        glfw.swap_buffers(window)

    glfw.terminate()
