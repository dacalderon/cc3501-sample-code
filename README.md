# CC3501 Sample Code

CC3501: Modeling and Computer Graphics for Engineers. Department of Computer Science, University of Chile.
This set of examples illustrate different computer graphics concepts in 2D and 3D while using: Python, OpenGL core profile, GLFW and Numpy.