# coding=utf-8
"""
Daniel Calderon, CC3501, 2019-2
Using mipmaps
"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys

from PIL import Image

import transformations as tr
import basic_shapes as bs
import easy_shaders as es

# A class to store the application control
class Controller:
    def __init__(self):
        self.fillPolygon = True


# global controller as communication with the callback function
controller = Controller()

def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return
    
    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_ESCAPE:
        sys.exit()


def textureWithMipMapSetup(texture, imgName):
    glBindTexture(GL_TEXTURE_2D, texture)

    # texture wrapping params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)

    # texture filtering params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    image = Image.open(imgName)
    img_data = np.array(list(image.getdata()), np.uint8)

    if image.mode == "RGB":
        internalFormat = GL_RGB
        format = GL_RGB
    elif image.mode == "RGBA":
        internalFormat = GL_RGBA
        format = GL_RGBA
    else:
        print("Image mode not supported.")
        raise Exception()

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, image.size[0], image.size[1], 0, format, GL_UNSIGNED_BYTE, img_data)
    glGenerateMipmap(GL_TEXTURE_2D)


if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 1200
    height = 600

    window = glfw.create_window(width, height, "No-Mipmap vs Mipmap", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Creating shader program
    pipeline = es.SimpleTextureTransformShaderProgram()

    # Setting up the clear screen color
    glClearColor(0.25, 0.25, 0.25, 1.0)

    # Creating shapes on GPU memory
    gpuAxis = es.toGPUShape(bs.createAxis(2))

    gpuShapeWithoutMipmap = es.toGPUShape(bs.createTextureQuad("red-woodpecker.jpg"), GL_REPEAT, GL_NEAREST)

    gpuShapeWithMipmap = es.toGPUShape(bs.createTextureQuad(None), GL_REPEAT, GL_NEAREST)
    textureWithMipMapSetup(gpuShapeWithMipmap.texture, "red-woodpecker.jpg")

    t0 = glfw.get_time()
    scale = 1.0

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        # Getting the time difference from the previous iteration
        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        if ((glfw.get_key(window, glfw.KEY_LEFT) == glfw.PRESS) or\
            (glfw.get_key(window, glfw.KEY_DOWN) == glfw.PRESS)) and\
            scale > 0.1:
            scale -= 2 * dt

        if ((glfw.get_key(window, glfw.KEY_RIGHT) == glfw.PRESS) or\
            (glfw.get_key(window, glfw.KEY_UP) == glfw.PRESS)) and\
            scale < 1.0:
            scale += 2 * dt

        # Clearing the screen
        glClear(GL_COLOR_BUFFER_BIT)

        # Drawing shapes
        glUseProgram(pipeline.shaderProgram)
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "transform"), 1, GL_TRUE, tr.matmul([
                tr.translate(-0.5, 0, 0),
                tr.scale(scale, 2*scale, 1)
                ]))
        pipeline.drawShape(gpuShapeWithoutMipmap)

        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "transform"), 1, GL_TRUE, tr.matmul([
                tr.translate(0.5, 0, 0),
                tr.scale(scale, 2*scale, 1)
                ]))
        pipeline.drawShape(gpuShapeWithMipmap)

        # Once the drawing is rendered, buffers are swap so an uncomplete drawing is never seen.
        glfw.swap_buffers(window)

    glfw.terminate()
